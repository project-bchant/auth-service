package com.bca.auth.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "userr")
public class Staff {
	@Id
	private String userid;
	
	private String nama;
	
	@Column(name="USERPASSWORD")
	private String password;
	
	private String email;
	
	private String flag;
	
	@Column(name="MENUACCESS")
	private String menuAccess;
	
	@Transient
	private String errorMessage;
	
	@Column(name="USERNAME")
	private String username;
	
	@Column(name="ISOWNER")
	private String isOwner;
	
	@Column(name="ISDELETED")
	private String isDeleted;
	
	@Transient
	private String response;
	
	public String getUserId() {
		return userid;
	}
	public void setUserId(String userId) {
		this.userid = userId;
	}
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public String getMenuAccess() {
		return menuAccess;
	}
	public void setMenuAccess(String menuAccess) {
		this.menuAccess = menuAccess;
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getIsOwner() {
		return isOwner;
	}
	public void setIsOwner(String isOwner) {
		this.isOwner = isOwner;
	}
	public String getIsDeleted() {
		return isDeleted;
	}
	public void setIsDeleted(String isDeleted) {
		this.isDeleted = isDeleted;
	}
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}
	@Override
	public String toString() {
		return "Staff [userid=" + userid + ", nama=" + nama + ", password=" + password + ", email=" + email + ", flag="
				+ flag + ", menuAccess=" + menuAccess + ", errorMessage=" + errorMessage + ", username=" + username
				+ ", isOwner=" + isOwner + ", isDeleted=" + isDeleted + ", response="
				+ response + "]";
	}
}
