package com.bca.auth.model;

import java.util.Date;

public interface Listmerchant {
	String getMerchantId();
	String getMerchantName();
	String getDetailLocation();
	Date getCreatedAt();
	String getStatus();
	String getLng();
	String getLat();
}
