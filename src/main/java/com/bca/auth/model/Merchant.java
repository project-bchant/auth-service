package com.bca.auth.model;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity
@Table(name = "merchant")
public class Merchant {
	
    @Id
    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "merchant_Sequence")
    @SequenceGenerator(name = "merchant_Sequence", allocationSize = 1, sequenceName = "MERCHANT_SEQ")
    @Column(name="merchantid")
    private Long merchantId;
	
	@NotBlank
	@Size(min=5, message="Merchant name should have atleast 5 characters.")
    @Column(name="merchantname")
	private String merchantName;
    
	@NotBlank
	@Size(min=3,max=50, message="Owner name should have atleast 5 characters.")
    @Column(name="ownername")
	private String ownerName;
    
	@NotBlank
    @Email
    @Size(max=30,message="Email reach maximum character , maximum is 30 characters.")
	private String email;
	
	@NotBlank
	@Size(min=5,max=30, message="username should have atleast 5 characters.")
	private String username;
	
	@NotBlank
	@Size(min=5, max=200, message="Detail location should have atleast 10 characters.")
	@Column(name="detaillocation")
	private String locationDetail;
	
	@NotBlank
	@Size(min=10, message="Account number should have atleast 10 numbers.")
	@Column(name="rekmerchant")
	private String merchantAccountNumber;
	
	@Column(name="createdat")
	private Date createdAt;
	
	@NotBlank
	@Size(max=30, message="Latitude reach maximum character , maximum is 12 characters.")
	private String lat;
	
	@NotBlank
	@Size(max=30, message="Longtitude reach maximum character , maximum is 12 characters.")
	private String lng;

	@NotBlank
	@Size(max=200,message="Area reach maximum character , maximum is 200 characters.")
	private String address;
	
	@NotBlank
	@Size(max=30,message="Area reach maximum character , maximum is 30 characters.")
	private String area;
	
	private String status;
	
	@NotBlank(message="Mobile number cannot be blank.")
	@Pattern(regexp="(^$|[0-9]{1,15})" , message ="Mobile number is wrong input. Must be number and maximum number is 15.")
	@Column(name="mobilenumber")
	private String mobileNumber;
	
	@Column(name="placeid")
	private String placeId;
	
	@Transient
	private String response;
	
	public Long getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(Long merchantId) {
		this.merchantId = merchantId;
	}

	public String getMerchantName() {
		return merchantName;
	}

	public void setMerchantName(String merchantName) {
		this.merchantName = merchantName;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getLocationDetail() {
		return locationDetail;
	}

	public void setLocationDetail(String locationDetail) {
		this.locationDetail = locationDetail;
	}

	public String getMerchantAccountNumber() {
		return merchantAccountNumber;
	}

	public void setMerchantAccountNumber(String merchantAccountNumber) {
		this.merchantAccountNumber = merchantAccountNumber;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

	public String getLng() {
		return lng;
	}

	public void setLng(String lng) {
		this.lng = lng;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getPlaceId() {
		return placeId;
	}

	public void setPlaceId(String placeId) {
		this.placeId = placeId;
	}

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}
}
