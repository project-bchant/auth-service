package com.bca.auth.payload;

public class LoginRequest {
    private String username;

    private String password;

    private String response;

    public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

	public String getResponse() {
		return response;
	}

	public void setResponse(String response) {
		this.response = response;
	}

	@Override
	public String toString() {
		return "LoginRequest [username=" + username + ", password=" + password + ", response=" + response + "]";
	}
}
