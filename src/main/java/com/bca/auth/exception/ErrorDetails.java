package com.bca.auth.exception;

import java.util.Date;

public class ErrorDetails {
	  private Date timestamp;
	  private String field;
	  private String message;
	  private String details;

	  public ErrorDetails(Date timestamp,String message, String field, String details) {
	    super();
	    this.timestamp = timestamp;
	    this.message = message;
	    this.field = field;
	    this.details = details;
	  }

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}
}
