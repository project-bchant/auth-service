package com.bca.auth.security;

import java.io.IOException;
import java.sql.Date;
import java.util.Collections;
import java.util.stream.Collectors;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.bca.auth.model.Staff;
import com.bca.auth.payload.ApiCaptchaResponse;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class JwtUsernameAndPasswordAuthenticationFilter extends UsernamePasswordAuthenticationFilter   {

	private AuthenticationManager authManager;
	
	private final JwtConfig jwtConfig;
	
	public JwtUsernameAndPasswordAuthenticationFilter(AuthenticationManager authManager, JwtConfig jwtConfig) {
		this.authManager = authManager;
		this.jwtConfig = jwtConfig;
		this.setRequiresAuthenticationRequestMatcher(new AntPathRequestMatcher("/login", "POST"));
	}
	
	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException {
		
		try {
			Staff creds = new ObjectMapper().readValue(request.getInputStream(), Staff.class);
			RestTemplate rest = new RestTemplate();
			final String url = "https://www.google.com/recaptcha/api/siteverify?"
		        		+ "secret="+jwtConfig.getGooglekey()+"&response="+creds.getResponse();
			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			HttpEntity<MultiValueMap<String, String>> headerG = new HttpEntity<>(headers);
			ResponseEntity<ApiCaptchaResponse> responses = rest.postForEntity(url, headerG , ApiCaptchaResponse.class);
			logger.info("Google captcha response : "+responses.getBody().getSuccess());
			if(responses.getBody().getSuccess().equals("false")) {
				logger.error("Captcha Error");
				throw new BadCredentialsException("Invalid credentials");
			}
			UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(
					creds.getUsername(), creds.getPassword(),Collections.emptyList());
			return authManager.authenticate(authToken);
			
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
			Authentication auth) throws IOException, ServletException {
		
		Long now = System.currentTimeMillis();
		UserPrincipal userPrincipal = (UserPrincipal) auth.getPrincipal();

		String token = Jwts.builder()
			.setSubject(auth.getName())	
			.claim("authorities", auth.getAuthorities().stream()
				.map(GrantedAuthority::getAuthority).collect(Collectors.toList()))
			.claim("menuAccess",userPrincipal.getMenuaccess())
			.claim("userId", userPrincipal.getUserId())
			.claim("merchantId", userPrincipal.getMerchantId())
			.claim("merchantName", userPrincipal.getMerchantName())
			.setIssuedAt(new Date(now))
			.setExpiration(new Date(now + jwtConfig.getExpiration() * 1000))
			.signWith(SignatureAlgorithm.HS512, jwtConfig.getSecret())
			.compact();
		
		response.addHeader(jwtConfig.getHeader(), jwtConfig.getPrefix() + token);
	}
}