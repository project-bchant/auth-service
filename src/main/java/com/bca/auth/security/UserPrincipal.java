package com.bca.auth.security;


import java.util.Collection;
import java.util.List;
import java.util.Objects;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;

import com.bca.auth.model.Staff;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class UserPrincipal implements UserDetails {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String id;

    private String name;

    private String username;
    
    private String menuaccess;
    
    @JsonIgnore
    private String email;

    @JsonIgnore
    private String password;
    
    private String merchantId;
    
    private String userId;
    
    private String merchantName;

    private Collection<? extends GrantedAuthority> authorities;

    public UserPrincipal(String id, String name, String username, 
    		String email, String password, String menuaccess,
    		Collection<? extends GrantedAuthority> authorities,
    		String merchantId , String userId) {
        this.id = id;
        this.name = name;
        this.username = username;
        this.email = email;
        this.password = password;
        this.menuaccess = menuaccess;
        this.authorities = authorities;
        this.merchantId = merchantId;
        this.userId = userId;
    }

    public static UserPrincipal create(Staff user) {
        List<GrantedAuthority> authorities = AuthorityUtils
            	.commaSeparatedStringToAuthorityList(user.getFlag());

        return new UserPrincipal(
                user.getUserId(),
                user.getNama(),
                user.getUsername(),
                user.getEmail(),
                user.getPassword(),
                user.getMenuAccess(),
                authorities,
                user.getFlag(),
                user.getUserId()
        );
    }
    
    public String getMerchantId() {
		return merchantId;
	}

	public String getUserId() {
		return userId;
	}

	public String getId() {
        return id;
    }

    public String getMerchantName() {
		return merchantName;
	}

	public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }
    
    public String getMenuaccess() {
		return menuaccess;
	}

	@Override
    public String getUsername() {
        return username;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserPrincipal that = (UserPrincipal) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }
}
