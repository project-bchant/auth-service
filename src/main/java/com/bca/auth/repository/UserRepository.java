package com.bca.auth.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bca.auth.model.Staff;

@Repository
public interface UserRepository extends JpaRepository<Staff, Long> {
    Optional<Staff> findByUsernameOrEmail(String username, String email);

    Optional<Staff> findByUsername(String username);

    Optional<Staff> findByEmail(String email);
    
    Boolean existsByUsername(String username);

    Boolean existsByEmail(String email);
}
