package com.bca.auth.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.bca.auth.model.Listmerchant;
import com.bca.auth.model.Merchant;

@Repository
public interface MerchantRepository extends JpaRepository<Merchant, Long> {
	
	@Query(value = "select MERCHANTID,MERCHANTNAME,DETAILLOCATION,CREATEDAT,STATUS,LNG,LAT from MERCHANT", nativeQuery=true)
	List<Listmerchant> listmerchant();
	
	@Query(value = "select MERCHANTID,MERCHANTNAME,DETAILLOCATION,CREATEDAT,STATUS,LNG,LAT from MERCHANT where STATUS = 'Approved' ", nativeQuery=true)
	List<Listmerchant> listmerchantapproved();
	
	@Query(value = "select case when count(*) > 0 then 'true' else 'false' end from merchant where status != 'Rejected' and username = ?1", nativeQuery=true)
    Boolean existsByUsername(String username);

	@Query(value = "select case when count(*) > 0 then 'true' else 'false' end from merchant where status != 'Rejected' and email = ?1", nativeQuery=true)
    Boolean existsByEmail(String email);
}

