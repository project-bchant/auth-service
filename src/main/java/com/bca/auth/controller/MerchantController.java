package com.bca.auth.controller;



import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.bca.auth.model.Listmerchant;
import com.bca.auth.model.Merchant;
import com.bca.auth.payload.ApiCaptchaResponse;
import com.bca.auth.payload.ApiResponse;
import com.bca.auth.payload.SignUpRequest;
import com.bca.auth.repository.MerchantRepository;
import com.bca.auth.repository.UserRepository;

@RestController
public class MerchantController {
	
	private static final Logger logger = LoggerFactory.getLogger(MerchantController.class);
	
    @Value("${google.secretkey}")
    private String secretkey;
	
    @Autowired
    UserRepository userRepository;
    
    @Autowired
    MerchantRepository merchantRepository;
    
    @Autowired
    PasswordEncoder passwordEncoder;
    
    @PostMapping("/register")
    public ResponseEntity<?> registerMerchant(@Valid @RequestBody Merchant merchant) {
		RestTemplate rest = new RestTemplate();
		logger.info(secretkey);
		final String url = "https://www.google.com/recaptcha/api/siteverify?"
	        		+ "secret="+secretkey+"&response="+merchant.getResponse();
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<MultiValueMap<String, String>> headerG = new HttpEntity<>(headers);
		ResponseEntity<ApiCaptchaResponse> responses = rest.postForEntity(url, headerG , ApiCaptchaResponse.class);

		if(responses.getBody().getSuccess().equals("false")) {
			throw new BadCredentialsException("Invalid credentials controller ");
		}
        if(merchantRepository.existsByUsername(merchant.getUsername())) {
        	logger.error("Merchant with username :"+merchant.getUsername() + " already taken");
            return new ResponseEntity<>(new ApiResponse(false, "Username is already taken!"),HttpStatus.BAD_REQUEST);
        }

        if(merchantRepository.existsByEmail(merchant.getEmail())) {
        	logger.error("Merchant with email :"+merchant.getUsername() + " already taken");
            return new ResponseEntity<>(new ApiResponse(false, "Email Address already in use!"),HttpStatus.BAD_REQUEST);
        }
    	merchant.setStatus("Waiting");
    	merchant.setCreatedAt(new Date());
    	merchantRepository.save(merchant);
    	logger.info("Merchant register success");
        return ResponseEntity.ok(new ApiResponse(true, "Merchant registered successfully"));
    }
    @GetMapping("/merchant")
    public List<Listmerchant> getMerchant() {
    	return merchantRepository.listmerchant();
    }
    
    @GetMapping("/merchant/approved")
    public List<Listmerchant> getMerchantapproved() {
    	return merchantRepository.listmerchantapproved();
    }
    
    @GetMapping("/merchant/{merchantid}")
    public Merchant getDetailmerchant(@PathVariable("merchantid") long id) {
    	logger.info("Detail merchant with ID: "+id);
    	return merchantRepository.findById(id).get();
    }
    
    @PostMapping("/check/email")
    public ResponseEntity<?> checkemail(@RequestBody SignUpRequest sur) {
        if(merchantRepository.existsByEmail(sur.getEmail())) {
        	logger.error(sur.getEmail()+" Email is already taken at merchant!");
            return new ResponseEntity<>(new ApiResponse(false, "Email is already taken!"),HttpStatus.BAD_REQUEST);
        }
        if(userRepository.existsByEmail(sur.getEmail())) {
        	logger.error(sur.getEmail()+" Email is already taken at user!");
            return new ResponseEntity<>(new ApiResponse(false, "Email is already taken!"),HttpStatus.BAD_REQUEST);
        }
        logger.info(sur.getEmail()+" Email is valid");
        return ResponseEntity.ok(new ApiResponse(true, "Email still valid."));
    }
    
    @PostMapping("/check/username")
    public ResponseEntity<?> checkusername(@RequestBody SignUpRequest sur) {
        if(merchantRepository.existsByUsername(sur.getUsername())) {
        	logger.error(sur.getUsername()+" Username is already taken at merchant!");
            return new ResponseEntity<>(new ApiResponse(false, "Username is already taken!"),HttpStatus.BAD_REQUEST);
        }
        if(userRepository.existsByUsername(sur.getUsername())) {
        	logger.error(sur.getUsername()+" Username is already taken at user!");
            return new ResponseEntity<>(new ApiResponse(false, "Username is already taken!"),HttpStatus.BAD_REQUEST);
        }
        logger.info(sur.getUsername()+" Username is valid");
        return ResponseEntity.ok(new ApiResponse(true, "Username still valid."));
    }
}
